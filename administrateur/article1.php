<?php

include("profil.php");
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/product.css">
    <title>Document</title>
</head>
<body>
    <nav>
        <a href="profil.php" class="logo">
            <h1>E-Wod Training</h1>
        </a>
        <div class="nav1">
            <ul>
                <li class="link">
                    <a href="article3.php">matériel de sport</a>
                   
                </li>

                <li class="link">
                    <a href="article2.php">homme</a>
                   
                </li>

                <li class="link">
                    <a href="article1.php">femme</a>
                   
                </li>

                <li class="link">
                    <a href="article4.php">accessoires</a>
                    
                </li>

                <li class="link">
                    <a href="#">programmes sportifs</a>
                    <ul class="submenu">
                        <li><a href="#"></a>Prise de masse</li>
                        <li><a href="#"></a>Home training</li>
                    </ul>
                </li>
            </ul>

            <div class="search-barre">
                <form>
                    <input type="search"placeholder="Rechercher">
                </form>
                <p>
                    <i class="fas fa-shopping-cart"></i>
                </p>
                <p>
                    <a class="user-login" href="login.php"><i class="fas fa-user"></i> </a>
                </p>
            </div>
        </div>
    </nav>
    <main>
        <div class="product">
            <div class="product-image">
                <img src="image/Femme/Débardeur.JPG" alt="">
            </div>
            <div class="product-info">
                <h3>Débardeur blanc femme</h3>
                <div class="product-note">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                </div>
                <span class="product-price">39.90 €</span>
                <span class="product-reference">Référence PNTS</span>
                <span class="product-size">M</span>
                <input type="number" name="quantity" class="product-quantity" placeholder="1">
                <button class="buy-btn">ajouter au panier</button>
                <p class="product-description">
                    Portez la débardeur avec classe, même hors de la salle de sport. Ce débardeur vous donne un look plus citadin, pous vos sorties entre amis.<br><br>
                    Toucher doux<br>
                    Col sympas<br>
                    Look cool<br>
                    Modèle près du corps<br>
                    Logo en bas et signature sur la manche brodés<br><br>

                    Le mannequin mesure 165 cm et porte du S<br>
                    Prendre une taille en dessous pour un rendu plus fit.<br><br>

                    Broderie Made in Occitanie<br><br>

                    95% Coton</p>
            </div>
        </div>
    </main>
    <footer>
        <div class="paiement">
                    <a href="#" class="fab fa-cc-mastercard"></a>
                    <a href="#" class="fab fa-cc-visa"></a>
                    <a href="#" class="fab fa-cc-paypal"></a>
        </div>
        <p>&copy; Projet réalisé par Nahman Mubashar</p>
        <div class="social-media">
            <p><i class="fab fa-facebook"></i></p>
            <p><i class="fab fa-twitter"></i></p>
            <p><i class="fab fa-instagram"></i></p>
            <p><i class="fab fa-youtube"></i></p>
        </div>

    </footer>
</body>

<script src="https://kit.fontawesome.com/eee9ee050b.js" crossorigin="anonymous"></script>
</html>