<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/fontawesome.min.css">
    <title>Wod Training</title>
</head>
<body>
    <nav>
        <a href="index.html" class="logo">
            <h1>E-Wod Training</h1>
        </a>
        <div class="nav1">
            <ul>
                <li class="link">
                    <a href="article3.php">matériel de sport</a>
                
                </li>

                <li class="link">
                    <a href="article2.php">homme</a>
                </li>

                <li class="link">
                    <a href="article1.php">femme</a>
                   
                </li>

                <li class="link">
                    <a href="article4.php">accessoires</a>
                </li>

                <li class="link">
                    <a href="#">programmes sportifs</a>
                    <ul class="submenu">
                        <li><a href="#"></a>Prise de masse</li>
                        <li><a href="#"></a>Home training</li>
                    </ul>
                </li>
            </ul>

            <div class="search-barre">
                <form>
                    <input type="search"placeholder="Rechercher">
                </form>
                <p>
                    <i class="fas fa-shopping-cart"></i>
                </p>
                <p>
                    <a href="login.php"><i class="fas fa-user"></i> </a>
                </p>
            </div>
        </div>
    </nav>
    <header>
        <h2>Bienvenue dans votre boutique.</h2>
        <button>Séance d'essaie<i class="fas fa-paper-plane"></i></button>
    </header>

    <section class="main">
        <div class="sliders">
            <img id="sld" src="image/slides/slide01.jpg">
        </div>

        <div class="cards">
            <div class="card">
                <img src="image/femme/Débardeur.JPG" alt="" class="">
                <span class="article-name">Débardeur Femme blanc</span>
                <span class="article-price">39.90€</span>
            </div>
            <div class="card">
                <img src="image/Homme/short.JPG" alt="" class="">
                <span class="article-name">Short de sport</span>
                <span class="article-price">19.90€</span>
            </div>
            <div class="card">
                <img src="image/Produits/Haltere.JPG" alt="" class="">
                <span class="article-name">Haltere 22.5kg</span>
                <span class="article-price">39.90€</span>
            </div>
            <div class="card">
                <img src="image/Accessoires/montre.jpg" alt="" class="">
                <span class="article-name">Montre de sport</span>
                <span class="article-price">49€</span>
            </div>
        </div>

        <div class="video">
            <iframe src="https://www.youtube.com/embed/C-qiilU0vIE" allowfullscreen></iframe>
        </div>
    </section>

    <footer>
        <div class="paiement">
                    <a href="#" class="fab fa-cc-mastercard"></a>
                    <a href="#" class="fab fa-cc-visa"></a>
                    <a href="#" class="fab fa-cc-paypal"></a>
        </div>
        <p>&copy; Projet réalisé par Nahman Mubashar</p>
        <div class="social-media">
            <p><i class="fab fa-facebook"></i></p>
            <p><i class="fab fa-twitter"></i></p>
            <p><i class="fab fa-instagram"></i></p>
            <p><i class="fab fa-youtube"></i></p>
        </div>

    </footer>

     <!-- Fichiers JS -->
    <script src="js/scripts.js"></script>
    <script src="https://kit.fontawesome.com/eee9ee050b.js" crossorigin="anonymous"></script>
</body>
</html>
